# https_dns_proxy

[Source code](https://github.com/aarond10/https_dns_proxy)

# docker image

[Github Repo](https://github.com/openwrt/docker)

# https_dns_proxy in openwrt

```sh
opkg update
opkg install https_dns_proxy dnsmasq
```
